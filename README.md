- 👋 Hi, I’m @nichromat
- 👀 I’m interested in Android development, AI development, and low level programming.
- 🌱 I’m currently learning Java for android and Git.
- 💞️ I’m looking to collaborate on nothing for the moment. I'm still learning :D
- 📫 How to reach me https://t.me/nichromat

<!---
nichromat/nichromat is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
